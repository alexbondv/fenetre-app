﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DaxxPersonApp.Entities.Models
{
    [Table("persons")]

    public class Person : BaseEntity
    {
        [Column("firstName")]
        [Required(ErrorMessage = "First name is not set")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Column("middleName")]
        public string MiddleName { get; set; }

        [Column("lastName")]
        [Required(ErrorMessage = "Last name is not set")]
        [StringLength(50)]
        public string LastName { get; set; }

        [Column("from")]
        [Required(ErrorMessage = "You should set From")]
        public DateTime From { get; set; }

        [Column("untill")]
        public DateTime Untill { get; set; }

        public Person():base()
        {

        }
    }
}
