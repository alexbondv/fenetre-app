﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DaxxPersonApp.Entities.Models
{
    public class BaseEntity
    {
        [Key]
        public int? Id { get; set; }
        public DateTime? CreationDate { get; set; }

        public BaseEntity()
        {
            CreationDate = DateTime.Now;
        }
    }
}
