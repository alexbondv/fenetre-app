﻿using DaxxPersonApp.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace DaxxPersonApp.Entities
{
    public class DaxxDataContext: DbContext
    {
        private readonly string _connectionString;

        public DaxxDataContext(DbContextOptions<DaxxDataContext> options) : base(options)
        {

        }
        public DaxxDataContext(string connectionString) : base()
        {
            _connectionString = connectionString;
        }
        public virtual DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {
                entity.Property(e => e.FirstName).IsRequired();
                entity.Property(e => e.LastName).IsRequired();
                entity.Property(e => e.From).IsRequired();

            });
            
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
