﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DaxxPersonApp.Entities.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<List<T>> GetAllAsync();
        Task<T> AddAsync(T obj);
        Task<T> UpdateAsync(T obj, int? id=null);
    }
}
