﻿using DaxxPersonApp.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DaxxPersonApp.Entities.Repositories
{
    public class PersonRepository : GenericRepository<Person>, IGenericRepository<Person>
    {
        public PersonRepository(DaxxDataContext context) : base(context)
        {

        }

        public async Task<List<Person>> GetAllPersonsAsync()
        {
            return await GetAllAsync();
        }

        public async Task<Person> AddPersonAsync(Person addedPerson)
        {        
            return await AddAsync(addedPerson);
        }

        public async Task<Person> UpdatePersonAsync(Person updatedPerson)
        {
            return await UpdateAsync(updatedPerson, updatedPerson.Id);
        }

    }
}
