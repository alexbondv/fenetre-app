﻿using System;
using System.Collections.Generic;
using System.Linq;
using DaxxPersonApp.Entities.Models;

namespace DaxxPersonApp.Entities
{
    public class DataInitializer
    {
        public static void Initialize(DaxxDataContext dbContext)
        {
            if (!dbContext.Persons.Any())
            {
                var persons = new List<Person>()
                {  new Person
                    {
                        FirstName = "Smith",
                        MiddleName = "Tomas",
                        LastName = "Carl",
                        From = DateTime.Now
                    },
                    new Person
                    {
                        FirstName = "Kennedy",
                        MiddleName = "Mary",
                        LastName = "Elizabeth",
                        From = DateTime.Now
                    },
                    new Person
                    {
                        FirstName = "Stuwart",
                        MiddleName = "Edward",
                        LastName = "John",
                        From = DateTime.Now
                    }
                };

                dbContext.Persons.AddRange(persons);
                dbContext.SaveChanges();
            }
        }
    }
}
