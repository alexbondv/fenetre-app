﻿using DaxxPersonApp.Entities;
using DaxxPersonApp.Entities.Models;
using DaxxPersonApp.Entities.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using DaxxPersonApp.Service.Shared;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using DaxxPersonApp.Service.ViewModels;

namespace DaxxPersonApp.Service
{
    public class PersonService
    {
        private readonly DaxxDataContext _context;
        private IDbConnection _dbConnection;
        private readonly PersonRepository _repository;
        private ResponseStatus _validationModel;

        public PersonService(string connection, ref ResponseStatus validationModel)
        {
            _dbConnection = new SqlConnection(connection);
            _context = new DaxxDataContext(connection);
            _repository = new PersonRepository(_context);
            _validationModel = validationModel;
        }

        public Task<List<Person>> GetAllPersonsAsync()
        {
            var persons = _repository.GetAllPersonsAsync();
            return persons;
        }
        public async Task<bool> AddPersonAsync(PersonViewModel view)
        {
            Person addedPerson = MapToModel(view);
            if (!ValidatePerson(addedPerson))
            {
                return false;
            }
            try
            {
                 await _repository.AddPersonAsync(addedPerson);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public async Task<bool> UpdatePersonAsync(PersonViewModel view)
        {
            Person updatedPerson = MapToModel(view);

            if (!ValidatePerson(updatedPerson))
            {
                return false;
            }

            try
            {
                await _repository.UpdatePersonAsync(updatedPerson);
            }
            catch
            {
                return false;
            }
            return true;
        }

        protected bool ValidatePerson(Person personToValidate)
        {
            if (personToValidate == null)
            {
                _validationModel.AddError("1", "Person is null");
            }
            if (personToValidate.From > personToValidate.Untill)
            {
                _validationModel.AddError("2", "Date from is bigger date untill");
            }

            if (string.IsNullOrEmpty(personToValidate.LastName))
            {
                _validationModel.AddError("3", "Last name is empty");
            }

            if (string.IsNullOrEmpty(personToValidate.FirstName))
            {
                _validationModel.AddError("4", "First name is empty");
            }
            return _validationModel.IsValid;
        }

        protected PersonViewModel MapToView(Person person)
        {
            var view = new PersonViewModel();
            view.Id = person.Id;
            view.FirstName = person.FirstName;
            view.LastName = person.LastName;
            view.MiddleName = person.MiddleName;
            view.From = person.From;
            view.Untill = person.Untill;
            return view;
        }
        protected Person MapToModel(PersonViewModel view)
        {
            var person = new Person();
            person.Id = view.Id;
            person.FirstName = view.FirstName;
            person.LastName = view.LastName;
            person.MiddleName = view.MiddleName;
            person.From = view.From;
            person.Untill = view.Untill;
            return person;
        }
    }
}
