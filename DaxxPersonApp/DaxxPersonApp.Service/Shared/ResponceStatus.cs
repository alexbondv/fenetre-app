﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DaxxPersonApp.Service.Shared
{
    public class ResponseStatus
    {
        public ModelStateDictionary ModelState;

        public ResponseStatus(ModelStateDictionary modelState)
        {
            ModelState = modelState;
        }

        public void AddError(string key, string errorMessage)
        {
            ModelState.AddModelError(key, errorMessage);
        }

        public bool IsValid
        {
            get { return ModelState.IsValid; }
        }
    }
}
