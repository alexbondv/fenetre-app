﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DaxxPersonApp.Service.ViewModels
{
    public class PersonViewModel
    {
        public int? Id { get; set; }
        [JsonRequired]
        [StringLength(50)]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        [JsonRequired]
        [StringLength(50)]
        public string LastName { get; set; }

        [JsonRequired]
        public DateTime From { get; set; }

        public DateTime Untill { get; set; }
    }
}
