﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DaxxPersonApp.Entities.Models;
using DaxxPersonApp.Service;
using DaxxPersonApp.Service.Shared;
using DaxxPersonApp.Service.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DaxxPersonApp.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly PersonService _service;
        private readonly ResponseStatus _validator;

        public PersonController(IConfiguration configuration)
        {
            _validator = new ResponseStatus(this.ModelState);
            _service = new PersonService(configuration.GetConnectionString("DBConnection"), ref _validator);
        }
        /// <summary>
        /// Gets a list of all persons.
        /// </summary>
        /// <remarks>
        /// Example request:
        ///     GET /api/person
        /// </remarks>
        /// <returns>Returns list persons</returns>
        /// <response code="200">Returns list persons</response>
        /// <response code="400">If connection to DB is wrong</response>  
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var view = await _service.GetAllPersonsAsync();
                return Ok(view);
            }
            catch (Exception ex)
            {
                return BadRequest( );

            }
        }
        /// <summary>
        /// Adds a new Person.
        /// </summary>
        /// /// <remarks>
        /// Example request:
        /// 
        ///     POST /api/person/add
        ///     {
        ///        "firstName": "FName",
        ///        "middleName": "MName",
        ///        "lastName": "LName",
        ///        "from":"2019-02-09T13:19:08.2362489",
        ///        "untill":"2019-02-11T00:00:00"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <response code="200">Successfully added person</response>
        /// <response code="400">Wrong validation. Error codes:1 - Person is null; 2 - Date from is bigger date untill; 3 - Last name is empty; 4 - First name is empty</response>
        [HttpPost("Add")]
        public async Task<IActionResult> Add([FromBody]PersonViewModel addedPerson)
        {
            try
            {
                if (!await _service.AddPersonAsync(addedPerson))
                    return BadRequest(_validator.ModelState);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
        }
        /// <summary>
        /// Updates a specific Person.
        /// </summary>
        /// <remarks>
        /// Example request:
        /// 
        ///     POST /api/person/update
        ///     {
        ///        "id": 1,
        ///        "firstName": "FName",
        ///        "middleName": "MName",
        ///        "lastName": "LName",
        ///        "from":"2019-02-09T13:19:08.2362489",
        ///        "untill":"2019-02-11T00:00:00"
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <response code="200">Successfully updated person</response>
        /// <response code="400">Wrong validation. Error codes:1 - Person is null; 2 - Date from is bigger date untill; 3 - Last name is empty; 4 - First name is empty</response>
        [HttpPost("Update")]
        public async Task<IActionResult> Update([FromBody]PersonViewModel updatedPerson)
        {
            try
            {
                if (!await _service.UpdatePersonAsync(updatedPerson))
                    return BadRequest(_validator.ModelState);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();

            }
        }
    }
}
